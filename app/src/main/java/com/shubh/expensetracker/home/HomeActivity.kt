package com.shubh.expensetracker.home

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.ViewPager.OnPageChangeListener
import com.shubh.expensetracker.R
import com.shubh.expensetracker.addEditTransaction.AddEditTransactionActivity
import com.shubh.expensetracker.core.activity.BaseActivity
import com.shubh.expensetracker.databinding.ActivityHomeBinding
import com.shubh.expensetracker.home.transaction.TransactionViewModel
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: TransactionViewModel

    private lateinit var activityMainBinding: ActivityHomeBinding

    private var mCurrentPosition = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView<ActivityHomeBinding>(this, R.layout.activity_home).apply {
            viewmodel = viewModel
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        viewPager.addOnPageChangeListener(mPageChangeListener)
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        addTransaction.setOnClickListener {
            val intent = Intent(this, AddEditTransactionActivity::class.java)
            startActivity(intent)
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                if (mCurrentPosition != 0) {
                    viewPager.setCurrentItem(0, true)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                if (mCurrentPosition != 1) {
                    viewPager.setCurrentItem(1, true)
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private val mPageChangeListener = object : OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {
            mCurrentPosition = position;
        }
    }
}
