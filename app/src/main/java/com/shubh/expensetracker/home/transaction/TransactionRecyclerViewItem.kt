package com.shubh.expensetracker.home.transaction

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.shubh.expensetracker.R
import com.shubh.expensetracker.core.adapter.RecyclerItemView
import com.shubh.expensetracker.data.Transaction
import com.shubh.expensetracker.databinding.TransactionItemBinding
import com.shubh.expensetracker.utils.BindableFieldTarget
import com.squareup.picasso.Picasso

class TransactionRecyclerViewItem(var transaction: Transaction, viewModel: TransactionViewModel) : RecyclerItemView {

    private var biddableFieldTarget: BindableFieldTarget? = null
    val image = ObservableField<Drawable>()

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<TransactionItemBinding>(layoutInflater, R.layout.transaction_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindView(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).transactionItemBinding.item = this
        holder.transactionItemBinding.listener = transactionItemUserActionsListener
        transaction.filePath?.let { loadImage(Uri.parse(it), holder.itemView.context) }
    }

    private fun loadImage(imageUri: Uri, context: Context) {
        biddableFieldTarget = BindableFieldTarget(image, context.resources)
        Picasso.with(context)
                .load(imageUri)
                .error(R.drawable.ic_photo)
                .placeholder(R.drawable.ic_photo)
                .into(biddableFieldTarget)
    }

    var transactionItemUserActionsListener = object : TransactionItemUserActionsListener {
        override fun onTransactionClicked(transaction: Transaction) {
            viewModel.openTaskEvent.postValue(transaction.id!!)
        }
    }

    class ViewHolder(var transactionItemBinding: TransactionItemBinding) : RecyclerView.ViewHolder(transactionItemBinding.root)
}