package com.shubh.expensetracker.home

import com.shubh.expensetracker.core.scope.PerFragment
import com.shubh.expensetracker.home.overview.OverviewFragment
import com.shubh.expensetracker.home.transaction.TransactionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [(HomeActivotyModule::class)])
abstract class FragmentModule {

    @ContributesAndroidInjector()
    @PerFragment
    protected abstract fun bindTransactionFragment(): TransactionFragment

    @ContributesAndroidInjector
    @PerFragment
    protected abstract fun bindOverviewFragment(): OverviewFragment


}