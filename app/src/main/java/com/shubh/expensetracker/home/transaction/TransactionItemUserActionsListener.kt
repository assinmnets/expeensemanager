package com.shubh.expensetracker.home.transaction


import com.shubh.expensetracker.data.Transaction

interface TransactionItemUserActionsListener {

    fun onTransactionClicked(transaction: Transaction)
}
