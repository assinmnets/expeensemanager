package com.shubh.expensetracker.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.shubh.expensetracker.home.overview.OverviewFragment
import com.shubh.expensetracker.home.transaction.TransactionFragment


class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> TransactionFragment()
            1 -> OverviewFragment()
            else -> throw IllegalStateException("Invalid position")
        }
    }

    override fun getCount(): Int {
        return 2
    }
}
