package com.shubh.expensetracker.home.overview

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.shubh.expensetracker.app.Injectable
import com.shubh.expensetracker.databinding.FragmentOverviewBinding
import com.shubh.expensetracker.home.transaction.TransactionViewModel
import com.shubh.expensetracker.utils.CollectionUtils
import kotlinx.android.synthetic.main.fragment_overview.*
import javax.inject.Inject


class OverviewFragment : Fragment(), Injectable {

    private lateinit var fragmentTransactionBinding: FragmentOverviewBinding

    @Inject
    lateinit var transactionViewModel: TransactionViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentTransactionBinding = FragmentOverviewBinding.inflate(inflater, container, false).apply {
            viewmodel = transactionViewModel
        }
        setHasOptionsMenu(true)
        return fragmentTransactionBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pieChart.setUsePercentValues(true)
        pieChart.description.isEnabled = false
        pieChart.setExtraOffsets(5F, 10F, 5F, 5F)

        pieChart.dragDecelerationFrictionCoef = 0.95f

        pieChart.setHoleColor(Color.WHITE)

        pieChart.setTransparentCircleColor(Color.WHITE)
        pieChart.setTransparentCircleAlpha(110)

        pieChart.holeRadius = 58f
        pieChart.transparentCircleRadius = 61f

        pieChart.setDrawCenterText(true)

        pieChart.rotationAngle = 0F
        pieChart.isRotationEnabled = true
        pieChart.isHighlightPerTapEnabled = true

        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad)


        val l = pieChart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        pieChart.setEntryLabelColor(Color.WHITE)
        pieChart.setEntryLabelTextSize(12f)

        fragmentTransactionBinding.viewmodel?.items?.observe(this, Observer { list ->
            run {
                if (CollectionUtils.isEmpty(list)) {
                    fragmentTransactionBinding.viewmodel?.empty?.set(true)
                    return@run
                }
                fragmentTransactionBinding.viewmodel?.empty?.set(false)
                val map = HashMap<String, Double>()
                list?.forEach {
                    var amount = map[it.category]
                    if (amount == null) {
                        amount = it.amount!!
                    } else {
                        amount += it.amount!!
                    }
                    map[it.category!!] = amount
                }
                setData(map)
            }
        })
    }

    private fun setData(map: HashMap<String, Double>) {

        val entries = ArrayList<PieEntry>()
        map.keys.forEach {
            entries.add(PieEntry(map[it]?.toFloat()!!, it))
        }
        val dataSet = PieDataSet(entries, "Expense Details")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        // add a lot of colors

        val colors = ArrayList<Int>()

        for (c in ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = colors

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        pieChart.data = data

        pieChart.highlightValues(null)
        pieChart.invalidate()
    }

}