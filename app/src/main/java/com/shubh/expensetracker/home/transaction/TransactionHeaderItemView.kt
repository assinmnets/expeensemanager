package com.shubh.expensetracker.home.transaction

import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.shubh.expensetracker.R
import com.shubh.expensetracker.core.adapter.RecyclerItemView
import com.shubh.expensetracker.databinding.TransactionHeaderItemBinding


class TransactionHeaderItemView(formatDate: String, amount: String) : RecyclerItemView {

    var title: ObservableField<String> = ObservableField(formatDate)
    var subtitle: ObservableField<String> = ObservableField(amount)

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val transactionHeaderItemBinding = DataBindingUtil.inflate<TransactionHeaderItemBinding>(layoutInflater, R.layout.transaction_header_item, parent, false)
        return ViewHolder(transactionHeaderItemBinding)
    }

    override fun onBindView(holder: RecyclerView.ViewHolder) {
        (holder as ViewHolder).binding.item = this
    }

    class ViewHolder(var binding: TransactionHeaderItemBinding) : RecyclerView.ViewHolder(binding.root)

    fun setTotalAmount(amount: Double) {
        subtitle.set(amount.toString())
    }
}
