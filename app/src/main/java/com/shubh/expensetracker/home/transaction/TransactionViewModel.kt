package com.shubh.expensetracker.home.transaction

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.graphics.drawable.Drawable
import com.shubh.expensetracker.SingleLiveEvent
import com.shubh.expensetracker.data.Transaction
import com.shubh.expensetracker.data.source.TransactionDataRepository
import com.shubh.expensetracker.utils.DateUtils
import io.realm.RealmResults
import java.util.*
import javax.inject.Inject


class TransactionViewModel
@Inject
constructor(
        context: Application,
        private var transactionDataRepository: TransactionDataRepository
) : AndroidViewModel(context) {

    var items = MutableLiveData<List<Transaction>>()
    val dataLoading = ObservableBoolean(false)
    val noTransactionsLabel = ObservableField<String>()
    val noTransactionIconRes = ObservableField<Drawable>()
    val empty = ObservableBoolean(false)
    val transactionaAddViewVisible = ObservableBoolean(true)
    val totalAmount = ObservableField<String>()

    val currentFilteringLabel = ObservableField<String>()
    internal val openTaskEvent = SingleLiveEvent<Int>()

    private lateinit var results: RealmResults<Transaction>
    private var calendar: Calendar = Calendar.getInstance()
    private var currentMonth = calendar[Calendar.MONTH]
    private var currentYear = calendar[Calendar.YEAR]

    fun start() {
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        loadExpenseTransaction(true)
    }

    fun loadExpenseTransaction(showLoadingUi: Boolean) {
        dataLoading.set(showLoadingUi)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val startDate = calendar.time;
        calendar.set(Calendar.MONTH, calendar[Calendar.MONTH] + 1)
        val endDate = calendar.time
        results = transactionDataRepository.getExpenseTransaction(startDate, endDate)
        results.addChangeListener(realmChangeListener)
        onDataReceived()
    }

    private var realmChangeListener = { results: RealmResults<Transaction> ->
        if (results.isLoaded && results.isValid) {
            onDataReceived()
        }
    }

    private fun onDataReceived() {
        dataLoading.set(false)
        calendar.set(Calendar.MONTH, currentMonth)
        calendar.set(Calendar.YEAR, currentYear)
        currentFilteringLabel.set(DateUtils.formatDate(calendar))
        items.postValue(results)
        var totalAmount = 0.0
        results.forEach { transition ->
            run {
                totalAmount += transition.amount!!
            }
        }
        this.totalAmount.set(totalAmount.toString())
    }

    fun onPreviousNextSelected() {
        if (currentMonth >= Calendar.DECEMBER) {
            return
        }
        currentMonth += 1
        calendar.set(Calendar.MONTH, currentMonth)
        val currentMonth = calendar.get(Calendar.MONTH)
        loadExpenseTransaction(true)
    }

    fun onPreviousMonthSelected() {
        if (currentMonth <= Calendar.JANUARY) {
            return
        }
        currentMonth -= 1
        calendar.set(Calendar.MONTH, currentMonth)
        loadExpenseTransaction(true)
    }
}