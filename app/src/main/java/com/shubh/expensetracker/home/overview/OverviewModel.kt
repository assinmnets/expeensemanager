package com.shubh.expensetracker.home.overview


import android.arch.lifecycle.ViewModel

import javax.inject.Inject

class OverviewModel @Inject
constructor() : ViewModel()
