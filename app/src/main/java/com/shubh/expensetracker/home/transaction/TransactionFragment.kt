package com.shubh.expensetracker.home.transaction

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shubh.expensetracker.R
import com.shubh.expensetracker.addEditTransaction.AddEditTransactionActivity
import com.shubh.expensetracker.app.Injectable
import com.shubh.expensetracker.core.adapter.ItemizedRecyclerAdapter
import com.shubh.expensetracker.core.adapter.RecyclerItemView
import com.shubh.expensetracker.core.fragments.BaseFragment
import com.shubh.expensetracker.databinding.FragmentTransactionBinding
import com.shubh.expensetracker.utils.CollectionUtils
import com.shubh.expensetracker.utils.DateUtils
import kotlinx.android.synthetic.main.fragment_transaction.*
import java.util.*
import javax.inject.Inject


class TransactionFragment : BaseFragment(), Injectable {

    private lateinit var fragmentTransactionBinding: FragmentTransactionBinding

    @Inject
    lateinit var transactionViewModel: TransactionViewModel

    lateinit var adapter: ItemizedRecyclerAdapter;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentTransactionBinding = FragmentTransactionBinding.inflate(inflater, container, false).apply {
            viewmodel = transactionViewModel
        }
        setHasOptionsMenu(true)
        return fragmentTransactionBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.adapter = ItemizedRecyclerAdapter()
        val itemDecorator = DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(activity!!, R.drawable.divider)!!)
        transactionListRecyclerView.addItemDecoration(itemDecorator)
        transactionListRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        transactionListRecyclerView.adapter = this.adapter
        fragmentTransactionBinding.viewmodel?.openTaskEvent?.observe(this, Observer { transitionId ->
            openTransactionDetails(transitionId)
        })
    }

    private fun openTransactionDetails(transitionId: Int?) {
        val intent = Intent(context, AddEditTransactionActivity::class.java).apply {
            putExtra(AddEditTransactionActivity.EXTRA_TRANSACTION_ID, transitionId)
        }
        startActivityForResult(intent, AddEditTransactionActivity.REQUEST_CODE)

    }

    override fun onResume() {
        super.onResume()
        fragmentTransactionBinding.viewmodel?.items?.observe(this, Observer { list ->
            run {
                if (CollectionUtils.isEmpty(list)) {
                    fragmentTransactionBinding.viewmodel?.empty?.set(true)
                    return@run
                }
                fragmentTransactionBinding.viewmodel?.empty?.set(false)
                list?.apply {
                    val calendar = Calendar.getInstance()
                    val dataList = mutableListOf<RecyclerItemView>()

                    var previousDay = -1
                    var todayTotalAmount = 0.0
                    var previousTransactionHeaderItemView: TransactionHeaderItemView? = null

                    list.forEach {
                        calendar.time = it.date
                        val today = calendar[Calendar.DAY_OF_MONTH]

                        if (today != previousDay) {
                            if (previousTransactionHeaderItemView != null) {
                                previousTransactionHeaderItemView!!.setTotalAmount(todayTotalAmount)
                                todayTotalAmount = it.amount!!
                            } else {
                                todayTotalAmount += it.amount!!
                            }
                            previousTransactionHeaderItemView = TransactionHeaderItemView(DateUtils.formatDate(it.date!!), todayTotalAmount.toString())
                            dataList.add(previousTransactionHeaderItemView!!)
                        }

                        previousDay = today
                        dataList.add(TransactionRecyclerViewItem(it, fragmentTransactionBinding.viewmodel!!))
                    }
                    adapter.items = dataList
                }
            }
        })
        fragmentTransactionBinding.viewmodel?.start()
    }

}