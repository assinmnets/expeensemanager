package com.shubh.expensetracker.home

import com.shubh.expensetracker.TransactionViewModelFactory
import com.shubh.expensetracker.core.scope.PerActivity
import com.shubh.expensetracker.home.transaction.TransactionViewModel
import dagger.Module
import dagger.Provides

@Module
class HomeActivotyModule {

    @Provides
    @PerActivity
    fun provideTransactionModule(viewModelFactory: TransactionViewModelFactory): TransactionViewModel {
        return viewModelFactory.create(TransactionViewModel::class.java)
    }
}