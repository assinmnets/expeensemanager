package com.shubh.expensetracker.data.source

import com.shubh.expensetracker.data.Transaction
import io.realm.RealmResults
import java.util.*

interface TransactionDataSource {

    fun getExpenseTransaction(startDate: Date, endDate: Date): RealmResults<Transaction>

    fun getTransaction(transactionId: Int): Transaction

    fun saveTransaction(newTransaction: Transaction)

    fun updateTransaction(transaction: Transaction)
}