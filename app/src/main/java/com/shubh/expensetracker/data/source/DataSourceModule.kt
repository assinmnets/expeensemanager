package com.shubh.expensetracker.data.source

import com.shubh.expensetracker.core.data.Local
import com.shubh.expensetracker.data.source.local.LocalTransactionDataSource
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataSourceModule {

    @Singleton
    @Binds
    @Local
    abstract fun provideTasksLocalDataSource(localTransactionDataSource: LocalTransactionDataSource): TransactionDataSource
}