package com.shubh.expensetracker.data

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import java.util.*

open class Transaction : RealmObject() {

    @PrimaryKey
    @Required
    var id: Int? = 0

    var filePath: String? = null
    @Required
    var category: String? = null

    var description: String? = null

    @Required
    var amount: Double? = null

    @Required
    var date: Date? = null
}