package com.shubh.expensetracker.data.source.local

import com.shubh.expensetracker.core.data.Local
import com.shubh.expensetracker.data.Transaction
import com.shubh.expensetracker.data.source.TransactionDataSource
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Local
@Singleton
class LocalTransactionDataSource @Inject constructor(var realm: Realm) : TransactionDataSource {

    override fun saveTransaction(newTransaction: Transaction) {
        realm.executeTransaction { realm ->
            run {
                val currentIdNum = realm.where(Transaction::class.java).max("id")
                val nextId: Int
                if (currentIdNum == null) {
                    nextId = 1
                } else {
                    nextId = currentIdNum.toInt() + 1
                }
                newTransaction.id = nextId
                realm.insert(newTransaction)
            }
        }
    }

    override fun updateTransaction(transaction: Transaction) {
        realm.executeTransaction { realm.insertOrUpdate(transaction) }
    }

    override fun getExpenseTransaction(startDate: Date, endDate: Date): RealmResults<Transaction> {
        return realm.where(Transaction::class.java).sort("date", Sort.DESCENDING).between("date", startDate, endDate)
                .findAll()
    }

    override fun getTransaction(transactionId: Int): Transaction {
        return realm.where(Transaction::class.java).equalTo("id", transactionId)
                .findFirstAsync()
    }


}