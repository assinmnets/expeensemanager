package com.shubh.expensetracker.data.source

import com.shubh.expensetracker.core.data.Local
import com.shubh.expensetracker.data.Transaction
import io.realm.RealmResults
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TransactionDataRepository
@Inject
constructor(
        @Local
        private var localTransactionDataSource: TransactionDataSource) : TransactionDataSource {

    override fun saveTransaction(newTransaction: Transaction) {
        localTransactionDataSource.saveTransaction(newTransaction)
    }

    override fun updateTransaction(transaction: Transaction) {
        localTransactionDataSource.updateTransaction(transaction)
    }

    override fun getExpenseTransaction(startDate: Date, endDate: Date): RealmResults<Transaction> {
        return localTransactionDataSource.getExpenseTransaction(startDate, endDate)
    }

    override fun getTransaction(transactionId: Int): Transaction {
        return localTransactionDataSource.getTransaction(transactionId)
    }

}