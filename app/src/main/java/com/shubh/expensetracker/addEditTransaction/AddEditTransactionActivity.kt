package com.shubh.expensetracker.addEditTransaction

import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import com.shubh.expensetracker.R
import com.shubh.expensetracker.core.activity.BaseActivity
import com.shubh.expensetracker.databinding.ActivityAddTransactionBinding
import com.shubh.expensetracker.utils.ADD_EDIT_RESULT_OK
import com.shubh.expensetracker.utils.replaceFragmentInActivity
import com.shubh.expensetracker.utils.setupActionBar
import javax.inject.Inject


class AddEditTransactionActivity : BaseActivity() {

    @Inject
    lateinit var addEditTransactionViewModel: AddEditTransactionViewModel

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityAddTransactionBinding>(this, R.layout.activity_add_transaction).apply {
            viewmodel = addEditTransactionViewModel
        }
        setupActionBar(R.id.toolbar) {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        replaceFragmentInActivity(obtainViewFragment(), R.id.contentFrame)
    }


    private fun obtainViewFragment() = supportFragmentManager.findFragmentById(R.id.contentFrame)
            ?: AddEditTransactionFragment.newInstance().apply {
                arguments = Bundle().apply {
                    putInt(AddEditTransactionFragment.ARGUMENT_EDIT_TRANSACTION_ID, intent.getIntExtra(EXTRA_TRANSACTION_ID, -1))
                }
            }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val EXTRA_TRANSACTION_ID = "TRANSACTION_ID"
        const val REQUEST_CODE = 1
    }
}