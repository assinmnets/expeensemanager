package com.shubh.expensetracker.addEditTransaction

import com.shubh.expensetracker.core.scope.PerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AddEditTransactionModule {

    @ContributesAndroidInjector()
    @PerFragment
    protected abstract fun bindTransactionFragment(): AddEditTransactionFragment
}
