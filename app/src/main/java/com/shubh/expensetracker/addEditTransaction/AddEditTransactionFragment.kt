package com.shubh.expensetracker.addEditTransaction

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.shubh.expensetracker.R
import com.shubh.expensetracker.app.Injectable
import com.shubh.expensetracker.databinding.FragmentAddTransactionBinding
import com.shubh.expensetracker.utils.ADD_EDIT_RESULT_OK
import com.shubh.expensetracker.utils.DateRangeLimiter
import com.shubh.expensetracker.utils.DateUtils
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.fragment_add_transaction.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class AddEditTransactionFragment : Fragment(), Injectable, DatePickerDialog.OnDateSetListener {

    private lateinit var viewDataBinding: FragmentAddTransactionBinding

    @Inject
    lateinit var addEditTransactionViewModel: AddEditTransactionViewModel

    private var dpd: DatePickerDialog? = null

    private lateinit var categoreis: List<String>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        categoreis = loadCategories(R.array.categories)
        val adapter = ArrayAdapter<String>(context, R.layout.spinner_item, categoreis)
        adapter.setDropDownViewResource(R.layout.spinner_drop_down_item)
        viewDataBinding.spinAdapterCategory = adapter
        date.setOnClickListener { onDateClicked() }
        transactionImage.setOnClickListener { showMediaPicker() }
        setupActionBar()
        subscribeToNavigationChanges()
        loadData()
    }

    private fun loadCategories(resourceId: Int): List<String> {
        val categories = ArrayList<String>()

        if (context != null) {
            val typedArrayCategories: TypedArray?
            try {
                typedArrayCategories = resources.obtainTypedArray(resourceId)
                var i = 0
                while (i < typedArrayCategories.length()) {
                    categories.add(typedArrayCategories.getString(i))
                    i++
                }
                typedArrayCategories.recycle()
            } catch (e: Resources.NotFoundException) {
                e.printStackTrace()
            }
        }
        return categories
    }

    private fun onDateClicked() {
        val now = Calendar.getInstance()
        dpd = DatePickerDialog.newInstance(
                this@AddEditTransactionFragment,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        )
        dpd?.version = DatePickerDialog.Version.VERSION_2
        dpd?.accentColor = Color.parseColor("#9C27B0")
        dpd?.setTitle(getString(R.string.select_date))
        dpd?.setDateRangeLimiter(DateRangeLimiter())
        dpd?.show(activity?.fragmentManager!!, "DatePickerDialog")
    }

    private fun loadData() {
        viewDataBinding.viewmodel?.start(arguments?.getInt(ARGUMENT_EDIT_TRANSACTION_ID))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_add_transaction, container, false)

        viewDataBinding = FragmentAddTransactionBinding.bind(root).apply {
            viewmodel = addEditTransactionViewModel
        }
        setHasOptionsMenu(true)
        return viewDataBinding.root
    }

    private fun setupActionBar() {
        (activity as AppCompatActivity).supportActionBar?.setTitle(
                if (arguments != null && arguments!!.get(ARGUMENT_EDIT_TRANSACTION_ID) != null)
                    R.string.edit_transaction
                else
                    R.string.add_transaction
        )
    }

    override fun onDateSet(view: DatePickerDialog, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.YEAR, year)
        viewDataBinding.viewmodel?.date?.set(DateUtils.formatDate(calendar.time))
    }


    private fun showMediaPicker() {
        val permissionCheck = ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_READ_CONTACTS)
        } else {
            launchMediaPicker()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            launchMediaPicker()
        } else {
            Toast.makeText(context, R.string.permission_denied, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Activity.RESULT_OK
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == Activity.RESULT_OK) {
            val uris = Matisse.obtainResult(data)
            viewDataBinding.viewmodel?.imageUri = uris.first()
        }
    }

    private fun subscribeToNavigationChanges() {
        addEditTransactionViewModel.transactionUpdatedEvent.observe(this, Observer {
            this@AddEditTransactionFragment.onTransactionSaved()
        })
    }

    private fun onTransactionSaved() {
        activity?.setResult(ADD_EDIT_RESULT_OK)
        activity?.finish()
    }

    private fun launchMediaPicker() {
        Matisse.from(this@AddEditTransactionFragment)
                .choose(MimeType.allOf())
                .countable(false)
                .capture(true)
                .captureStrategy(CaptureStrategy(true, "com.shubh.expensetracker.fileprovider"))
                .maxSelectable(1)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .imageEngine(PicassoEngine())
                .forResult(REQUEST_CODE_CHOOSE)
    }

    companion object {
        const val MY_PERMISSIONS_REQUEST_READ_CONTACTS = 101
        const val REQUEST_CODE_CHOOSE = 102
        const val ARGUMENT_EDIT_TRANSACTION_ID = "TRANSACTION_ID"
        fun newInstance() = AddEditTransactionFragment()
    }
}
