package com.shubh.expensetracker.addEditTransaction

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.*
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.AppCompatSpinner
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.shubh.expensetracker.R
import com.shubh.expensetracker.SingleLiveEvent
import com.shubh.expensetracker.app.App
import com.shubh.expensetracker.data.Transaction
import com.shubh.expensetracker.data.source.TransactionDataRepository
import com.shubh.expensetracker.utils.BindableFieldTarget
import com.shubh.expensetracker.utils.DateUtils
import com.squareup.picasso.Picasso
import java.util.*
import javax.inject.Inject


@BindingAdapter(value = ["bind:selectedCategory", "bind:selectedCategoryAttrChanged"], requireAll = false)
fun bindCategorySelected(spinner: AppCompatSpinner, categorySetByViewModel: String?, inverseBindingListener: InverseBindingListener) {

    var initialSelectedCategory: String? = null
    if (spinner.adapter == null && categorySetByViewModel != null) {
        initialSelectedCategory = categorySetByViewModel
    }

    spinner.onItemSelectedListener = AddEditTransactionViewModel.SpinCategoryOnItemSelectedListener(initialSelectedCategory, inverseBindingListener)

    if (categorySetByViewModel != null && categorySetByViewModel != spinner.selectedItem) {
        val positionInAdapter = AddEditTransactionViewModel.getCategoryPositionInAdapter(spinner.adapter as ArrayAdapter<String>, categorySetByViewModel)
        if (positionInAdapter != null) {
            spinner.setSelection(positionInAdapter)
        }
    }
}

@InverseBindingAdapter(attribute = "bind:selectedCategory", event = "bind:selectedCategoryAttrChanged")
fun captureSelectedCategory(spinner: AppCompatSpinner): String {
    return spinner.selectedItem as String
}

@BindingConversion
fun convertCategoryToString(category: String?): String? {
    return category
}

class AddEditTransactionViewModel
@Inject
constructor(
        context: Application,
        private val transactionDataRepository: TransactionDataRepository
) : AndroidViewModel(context) {

    private var transactionId: Int = 0

    val description = ObservableField<String>()
    val amount = ObservableField<String>()
    val image = ObservableField<Drawable>()
    val date = ObservableField<String>()
    val dataLoading = ObservableBoolean(false)
    val selectedCategory = ObservableField<String>()

    val transactionUpdatedEvent = SingleLiveEvent<Boolean>()
    private var isDataLoaded = false
    private var bindableFieldTarget: BindableFieldTarget? = null
    private var isNewTask = false
        get() = transactionId <= 0

    var imageUri: Uri? = null
        set(value) {
            if (value != null) {
                loadImage(value)
            }
        }


    fun start(transactionId: Int?) {
        if (dataLoading.get()) {
            return
        }

        if (transactionId != null) {
            this.transactionId = transactionId
        }

        if (isNewTask || isDataLoaded) {
            setupNewTransaction()
            return
        }

        loadTransaction()
    }

    private fun setupNewTransaction() {
        val now = Calendar.getInstance()
        val currentDate = DateUtils.formatDate(now.time)
        date.set(currentDate)
        image.set(ActivityCompat.getDrawable(getApplication(), R.drawable.ic_photo))
    }

    private fun loadTransaction() {
        dataLoading.set(true)

        transactionId.let {
            val transaction = transactionDataRepository.getTransaction(it)
            transaction.load()
            selectedCategory.set(transaction.category)
            description.set(transaction.description)
            amount.set(transaction.amount.toString())
            date.set(DateUtils.formatDate(transaction.date!!))
            imageUri = Uri.parse(transaction.filePath)
            imageUri?.let {
                loadImage(it)
            }
            isDataLoaded = true
            dataLoading.set(false)
        }
    }

    private fun loadImage(imageUri: Uri) {
        bindableFieldTarget = BindableFieldTarget(image, getApplication<App>().resources)
        Picasso.with(getApplication<App>())
                .load(imageUri)
                .placeholder(R.drawable.ic_photo)
                .error(R.drawable.ic_photo)
                .into(bindableFieldTarget)
    }

    fun onSaveClick() {
        val transaction = Transaction()

        if (amount.get() == null) {
            Toast.makeText(getApplication(), R.string.error_no_amount, Toast.LENGTH_SHORT).show()
            return
        }

        transaction.filePath = imageUri.toString()
        transaction.category = selectedCategory.get()
        transaction.description = description.get()
        transaction.date = DateUtils.parseDate(date.get())
        transaction.amount = amount.get().toDouble()

        if (isNewTask) {
            transactionDataRepository.saveTransaction(transaction)
        } else {
            transactionId.let {
                transaction.id = it
                transactionDataRepository.updateTransaction(transaction)
            }
        }
        if (isNewTask) {
            transactionUpdatedEvent.postValue(true)
        }
    }

    open class SpinCategoryOnItemSelectedListener(
            private var initialSelectedCategory: String?,
            private val inverseBindingListener: InverseBindingListener?) : AdapterView.OnItemSelectedListener {

        override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
            if (initialSelectedCategory != null) {
                val positionInAdapter = getCategoryPositionInAdapter(adapterView.adapter as ArrayAdapter<String>, initialSelectedCategory)
                if (positionInAdapter != null) {
                    adapterView.setSelection(positionInAdapter)
                }
                initialSelectedCategory = null
            } else {
                inverseBindingListener?.onChange()
            }
        }

        override fun onNothingSelected(adapterView: AdapterView<*>) {}
    }

    companion object {
        fun getCategoryPositionInAdapter(adapter: ArrayAdapter<String>?, category: String?): Int? {
            if (adapter != null && category != null) {
                for (i in 0 until adapter.count) {
                    val adapterCategory = adapter.getItem(i)
                    if (TextUtils.isEmpty(adapterCategory) && adapterCategory!! == category) {
                        return i
                    }
                }
            }
            return null
        }
    }
}
