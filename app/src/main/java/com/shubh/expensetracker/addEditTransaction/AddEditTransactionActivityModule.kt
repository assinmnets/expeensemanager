package com.shubh.expensetracker.addEditTransaction

import android.arch.lifecycle.ViewModelProvider
import com.shubh.expensetracker.core.scope.PerActivity
import dagger.Module
import dagger.Provides

@Module
class AddEditTransactionActivityModule {

    @Provides
    @PerActivity
    fun provideAddEditTransactionViewModel(viewModelFactory: ViewModelProvider.Factory): AddEditTransactionViewModel {
        return viewModelFactory.create(AddEditTransactionViewModel::class.java)
    }
}
