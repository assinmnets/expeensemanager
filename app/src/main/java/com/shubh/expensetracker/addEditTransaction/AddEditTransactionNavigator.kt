package com.shubh.expensetracker.addEditTransaction

internal interface AddEditTransactionNavigator {

    fun onTransactionSaved()
}
