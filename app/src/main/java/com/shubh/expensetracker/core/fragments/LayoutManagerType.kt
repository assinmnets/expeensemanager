package com.shubh.expensetracker.core.fragments

enum class LayoutManagerType {
    GRID_LAYOUT_MANAGER,
    HORIZONTAL_LINEAR_LAYOUT_MANAGER,
    LINEAR_LAYOUT_MANAGER
}