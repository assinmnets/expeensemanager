package com.shubh.expensetracker.core.fragments

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.shubh.expensetracker.core.adapter.ItemizedRecyclerAdapter
import com.shubh.expensetracker.core.adapter.RecyclerAdapter
import com.shubh.expensetracker.core.adapter.RecyclerItemView

abstract class RecyclerViewFragment : BaseFragment() {

    private lateinit var mCurrentLayoutManagerType: LayoutManagerType

    protected open var recyclerView: RecyclerView? = null

    private lateinit var mLayoutManager: RecyclerView.LayoutManager

    /*Abstract methods*/
    protected abstract val layoutManagerType: LayoutManagerType

    /**
     * If Default implementation need to be overridden then override this method and provide custom implementation
     *
     * @return - #{@RecyclerAdapter}
     */
    protected val adapter: RecyclerAdapter
        get() = ItemizedRecyclerAdapter()

    protected val spanCount: Int
        get() = SPAN_COUNT

    /**
     * Will be called when any item of recycler view has been clicked
     *
     * @param recyclerView @{RecyclerView}
     * @param position     - Item position
     * @param view-        view which has been clicked
     */
    protected fun OnItemClicked(recyclerView: RecyclerView, position: Int, view: View) {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mLayoutManager = LinearLayoutManager(activity)

        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER

        if (savedInstanceState != null) {
            mCurrentLayoutManagerType = savedInstanceState.getSerializable(KEY_LAYOUT_MANAGER) as LayoutManagerType
        }


        setRecyclerViewLayoutManager(layoutManagerType)
        recyclerView?.adapter = this.adapter

        // ItemClickSupport.addTo(recyclerView).setOnItemClickListener({ recyclerView, position, view -> this.OnItemClicked(recyclerView, position, view) })
    }

    private fun setRecyclerViewLayoutManager(layoutManagerType: LayoutManagerType) {
        var scrollPosition = 0

        if (recyclerView?.layoutManager != null) {
            scrollPosition = (recyclerView!!.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        }

        recyclerView?.setHasFixedSize(true)

        when (layoutManagerType) {
            LayoutManagerType.GRID_LAYOUT_MANAGER -> {
                mLayoutManager = GridLayoutManager(activity, spanCount)
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER
            }
            LayoutManagerType.HORIZONTAL_LINEAR_LAYOUT_MANAGER -> {
                mLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER
            }
            LayoutManagerType.LINEAR_LAYOUT_MANAGER -> {
                mLayoutManager = LinearLayoutManager(activity)
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER
            }
        }
        recyclerView?.layoutManager = mLayoutManager
        recyclerView?.scrollToPosition(scrollPosition)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType)
        super.onSaveInstanceState(savedInstanceState)
    }

    fun addItems(recyclerItemViews: MutableList<RecyclerItemView>) {
        adapter.items = recyclerItemViews
    }

    companion object {

        private const val KEY_LAYOUT_MANAGER = "layoutManager"

        private const val SPAN_COUNT = 2
    }
}
