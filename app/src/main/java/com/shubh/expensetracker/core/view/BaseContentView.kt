package com.shubh.expensetracker.core.view

interface BaseContentView : BaseView {

    fun showProgress()

    fun hideProgress()

    fun showRetry()

    fun hideRetry()

    fun hideContent()

    fun showContent()

    fun showEmptyScreen()

    fun showError()
}
