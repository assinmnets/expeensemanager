package com.shubh.expensetracker.core.presenter


import com.shubh.expensetracker.core.view.BaseView

interface BasePresenter<V : BaseView> {

    val view: V

    fun onViewAttached(view: V)

    fun onViewDetached()
}
