package com.shubh.expensetracker.core.adapter

import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import com.shubh.expensetracker.utils.CollectionUtils
import java.util.*
import java.util.concurrent.atomic.AtomicInteger


abstract class RecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val nextId = AtomicInteger(1)
    private var mClassVsViewTypeLookup: MutableMap<Class<*>, Int> = HashMap()
    protected var mViewTypeVsViewItemLookup = SparseArray<RecyclerItemView>()
    protected var mItems: MutableList<RecyclerItemView> = mutableListOf()

    var items: MutableList<RecyclerItemView>
        get() = mItems
        set(items) {
            mItems = items
            computeViewType()
            notifyDataSetChanged()
        }

    init {
        mItems = ArrayList()
    }

    override fun getItemViewType(position: Int): Int {
        if (CollectionUtils.isEmpty(mItems)) {
            return 0
        }

        val item = mItems[position]
        return mClassVsViewTypeLookup[item.javaClass]!!
    }

    private fun computeViewType() {
        if (CollectionUtils.isEmpty(mItems)) {
            return
        }

        for (recyclableViewItem in mItems) {
            val classs = recyclableViewItem.javaClass
            if (!mClassVsViewTypeLookup.containsKey(classs)) {
                mClassVsViewTypeLookup[classs] = nextId.get()
                mViewTypeVsViewItemLookup.put(nextId.getAndIncrement(), recyclableViewItem)
            }
        }
    }
    override fun getItemCount(): Int {
        return mItems.size
    }

    fun addItems(items: List<RecyclerItemView>) {
        val oldItemCount = mItems.size
        mItems.addAll(items)
        computeViewType()
        //Notify only changed items
        notifyItemRangeInserted(oldItemCount, items.size)
    }

    fun <T> getItem(position: Int): T {
        return mItems[position] as T
    }
}
