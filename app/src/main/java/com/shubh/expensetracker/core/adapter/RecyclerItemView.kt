package com.shubh.expensetracker.core.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * Created by : Subham Tyagi
 * Created on :  28/08/16.
 */

interface RecyclerItemView {

    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindView(holder: RecyclerView.ViewHolder)
}
