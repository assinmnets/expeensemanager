package com.shubh.expensetracker.core.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * Created by : Subham Tyagi
 * Created on :  28/08/16.
 */

class ItemizedRecyclerAdapter : RecyclerAdapter() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = mViewTypeVsViewItemLookup.get(viewType)
        return itemView.onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemView = mItems[position]
        itemView.onBindView(holder)
    }
}
