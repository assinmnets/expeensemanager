package com.shubh.expensetracker.core.presenter


import com.shubh.expensetracker.core.view.BaseView
import timber.log.Timber
import java.lang.ref.WeakReference

abstract class AbsPresenter<V : BaseView>(view: V) {

    private var mViewReference: WeakReference<V>? = null

    val view: V?
        get() {
            if (mViewReference == null) {
                throw IllegalStateException("Presenter is not attached to view. Call onViewAttached from activity/fragment ")
            }
            return mViewReference!!.get()
        }

    init {
        Timber.tag(javaClass.simpleName)
        mViewReference = WeakReference(view)
    }

    fun onViewAttached(view: V) {
        mViewReference = WeakReference(view)
    }

    fun onViewDetached() {
        if (mViewReference != null) {
            mViewReference!!.clear()
            mViewReference = null
        }
    }
}
