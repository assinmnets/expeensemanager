package com.shubh.expensetracker.utils

import io.realm.RealmModel
import io.realm.RealmResults

// Convenience extension on RealmResults to return as LiveRealmData
public fun <T : RealmModel> RealmResults<T>.asLiveData() = RealmLiveData(this)