package com.shubh.expensetracker.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private val DATE_FORMAT = DateFormat.getDateInstance()

    fun formatDate(date: Date): String {
        return DATE_FORMAT.format(date)
    }

    fun parseDate(dateString: String): Date? {
        return try {
            DATE_FORMAT.parse(dateString)
        } catch (e: ParseException) {
            null
        }

    }

    fun formatDate(calendar: Calendar?): String? {
        var dateFormatter = SimpleDateFormat("MMM yyyy", Locale.getDefault())
        return dateFormatter.format(calendar?.time)
    }
}
