package com.shubh.expensetracker.utils


object CollectionUtils {
    fun isEmpty(items: Collection<*>?): Boolean {
        return items == null || items.isEmpty()
    }
}
