package com.shubh.expensetracker.utils

import android.os.Parcel
import android.os.Parcelable
import com.wdullaer.materialdatetimepicker.date.DateRangeLimiter
import java.util.*


internal class DateRangeLimiter() : DateRangeLimiter {

    constructor(parcel: Parcel) : this()

    override fun getMinYear(): Int {
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.YEAR)
    }

    override fun getMaxYear(): Int {
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.YEAR)
    }

    override fun getStartDate(): Calendar {
        val output = Calendar.getInstance()
        output.set(Calendar.DAY_OF_MONTH, 1)
        output.set(Calendar.MONTH, Calendar.JANUARY)
        return output
    }

    override fun getEndDate(): Calendar {
        val output = Calendar.getInstance()
        output.set(Calendar.DAY_OF_MONTH, 31)
        output.set(Calendar.MONTH, Calendar.DECEMBER)
        return output
    }

    override fun isOutOfRange(year: Int, month: Int, day: Int): Boolean {
        return false
    }

    override fun setToNearestDate(day: Calendar): Calendar {
        return day
    }


    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {

    }

    companion object {

        val CREATOR: Parcelable.Creator<com.shubh.expensetracker.utils.DateRangeLimiter> = object : Parcelable.Creator<com.shubh.expensetracker.utils.DateRangeLimiter> {
            override fun createFromParcel(parcel: Parcel): com.shubh.expensetracker.utils.DateRangeLimiter {
                return DateRangeLimiter(parcel)
            }

            override fun newArray(size: Int): Array<com.shubh.expensetracker.utils.DateRangeLimiter?> {
                return arrayOfNulls(size)
            }
        }
    }
}