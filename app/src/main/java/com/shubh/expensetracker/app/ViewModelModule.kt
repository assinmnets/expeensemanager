package com.shubh.expensetracker.app

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.shubh.expensetracker.TransactionViewModelFactory
import com.shubh.expensetracker.addEditTransaction.AddEditTransactionViewModel
import com.shubh.expensetracker.home.transaction.TransactionViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(TransactionViewModel::class)
    internal abstract fun bindTransactionViewModel(transactionViewModel: TransactionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddEditTransactionViewModel::class)
    internal abstract fun bindAddEditTransactionViewModel(addEditTransactionViewModel: AddEditTransactionViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: TransactionViewModelFactory): ViewModelProvider.Factory
}