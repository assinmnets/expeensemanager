package com.shubh.expensetracker.app

import android.app.Application
import android.content.Context
import com.shubh.expensetracker.data.source.DataSourceModule
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import io.realm.Realm
import javax.inject.Singleton

@Module(includes = [
    (AndroidInjectionModule::class),
    (AndroidSupportInjectionModule::class),
    (ActivityModule::class),
    (ViewModelModule::class),
    (DataSourceModule::class)])
internal class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideApplication(app: App): Application {
        return app
    }

    @Provides
    @Singleton
    fun provideRealm(): Realm {
        return Realm.getDefaultInstance()
    }
}
