package com.shubh.expensetracker.app

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber
import javax.inject.Inject


class App : Application(), HasActivityInjector {

    @Inject
    protected lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
        Timber.plant(Timber.DebugTree())
        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
                .name("expense.realm")
                .schemaVersion(0)
                .build()
        Realm.setDefaultConfiguration(realmConfig)
    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return activityInjector
    }
}
