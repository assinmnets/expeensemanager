package com.shubh.expensetracker.app


import com.shubh.expensetracker.addEditTransaction.AddEditTransactionActivity
import com.shubh.expensetracker.addEditTransaction.AddEditTransactionModule
import com.shubh.expensetracker.core.scope.PerActivity
import com.shubh.expensetracker.home.HomeActivity
import com.shubh.expensetracker.home.FragmentModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [(FragmentModule::class)])
    @PerActivity
    protected abstract fun bindStatisticsActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [(AddEditTransactionModule::class)])
    @PerActivity
    protected abstract fun bindAddEditTransactionActivity(): AddEditTransactionActivity
}

