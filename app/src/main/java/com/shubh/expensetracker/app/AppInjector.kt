package com.shubh.expensetracker.app

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import com.shubh.expensetracker.utils.ActivityLifeCycleCallbackAdapter
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector

/**
 * Helper class to automatically inject fragments if they implement [Injectable].
 */
object AppInjector {

    fun init(app: App) {
        DaggerAppComponent.builder().application(app).build().inject(app)
        app.registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks)
    }

    private fun handleActivity(activity: Activity?) {
        if (activity == null) {
            return
        }

        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }

        (activity as? FragmentActivity)?.supportFragmentManager?.registerFragmentLifecycleCallbacks(mFragmentLifecycleCallbacks, true)
    }

    var mActivityLifecycleCallbacks = object : ActivityLifeCycleCallbackAdapter() {
        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
            handleActivity(activity)
        }
    }

    var mFragmentLifecycleCallbacks = object : FragmentManager.FragmentLifecycleCallbacks() {
        override fun onFragmentCreated(fm: FragmentManager?, f: Fragment?, savedInstanceState: Bundle?) {
            if (f is Injectable) {
                AndroidSupportInjection.inject(f)
            }
        }
    }
}